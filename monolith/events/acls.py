from .keys import PEXELS_API_KEY, OPEN_WEATHER_KEY
import requests
import json


def get_photo(city, state):
    # Create a dictionary for the headers to use in the request
    headers = {"Authorization": PEXELS_API_KEY}
    params = {"query": city}
    # Note: I took state out because the city alone returns better results.
    # Create the URL for the request with the city and state
    # requestURL = f"https://api.pexels.com/v1/search?query={city}, {state}"
    # Note: I also just added the query to the address so no params needed.
    request_url = "https://api.pexels.com/v1/search?query={city}"
    # Make the request
    response = requests.get(request_url, params, headers=headers)

    # data = response.json()

    # # Extract the URL of one of the photos from the response
    # picture_url = data["photos"][0]["src"]["small"]

    # # Return a dictionary that contains a `picture_url` key and the URL of the photo
    # return {"picture_url": picture_url}


    content=response.content
    parsed_json = json.loads(content)
    picture = parsed_json["photos"][0]["src"]["medium"]
    return {
        "picture_url" : picture
    }



def get_lat_long(location):
    """
    Returns the latitude and longitude for the specified
    location using OPenWeatherMap API.
    """
    base_url = "http://api.openweathermap.org/geo/1.0/direct"
    params = {
        "q": f"{location.city}, {location.state.abbreviation}, USA",
        "appid": OPEN_WEATHER_KEY
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    return {
        "latitude": parsed_json[0]["lat"],
        "longitude": parsed_json[0]["lon"]
    }


def get_weather_data(location):

    lat_long = get_lat_long(location)

    base_url = "https://api.openweathermap.org/data/2.5/weather"

    params = {
        "lat": lat_long["latitude"],
        "lon": lat_long["longitude"],
        "appid": OPEN_WEATHER_KEY,
        "units": "imperial"
    }
    response = requests.get(base_url, params=params)
    parsed_json = json.loads(response.content)
    weather_data = {
        "temp": parsed_json["main"]["temp"],
        "description": parsed_json["weather"][0]["description"]
    }

    return weather_data
